import { StatusBar } from 'expo-status-bar';
import React, { useState } from "react";
import { Keyboard, KeyboardAvoidingView, Platform, SafeAreaView, ScrollView, SectionList, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Constants from 'expo-constants';
import Task from './components/Task';
export default function App() {

  const [task, setTask] = useState("");
  const [listTask, setLisTask] = useState([]);

  const handleAddTask = () => {
    console.log(task);
    Keyboard.dismiss();
    if (task !== "") {
      setLisTask([...listTask, {title:"",data:[task]}]);
      setTask("");
    }
  }

  const completeTask = (index) => {
    let itemsCopy = [...listTask];
    itemsCopy.splice(index, 1);
    setLisTask(itemsCopy);
  }

  const DATA = [
    {
      title: "Main dishes",
      data: ["Pizza", "Burger", "Risotto"]
    },
    {
      title: "Sides",
      data: ["French Fries", "Onion Rings", "Fried Shrimps"]
    },
    {
      title: "Drinks",
      data: ["Water", "Coke", "Beer"]
    },
    {
      title: "Desserts",
      data: ["Cheese Cake", "Ice Cream"]
    }
  ];
  
  return (
    <View style={styles.container}>
      {/* title */}
      <View style={styles.taskWrapper}>
        <Text style={styles.sectionTitle}>Tareas para Hoy!</Text>
        <SafeAreaView>
          <SectionList
          style={{height:'70vh', width:'90vw', alignContent:'center'}}
            sections={listTask}
            keyExtractor={(item, index) => item + index}
            renderItem={({ item,index }) => <TouchableOpacity key={index} onPress={() => completeTask(index)}>
            <Task text={item} />
          </TouchableOpacity>}
          >
            {/* <View style={styles.items}>
              {listTask.map((item, index) => {
                return (<TouchableOpacity key={index} onPress={() => completeTask(index)}>
                  <Task text={item} />
                </TouchableOpacity>);
              })}
            </View> */}
          </SectionList>
        </SafeAreaView>
      </View>
      {/* write a task */}
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.writeTaskWrapper}
      >
        <TextInput onKeyPress={(e) => { e.code == 'Enter'?handleAddTask():console.log('other',e); }} style={styles.input} placeholder={'escriba su tarea!'} value={task} onChangeText={text => setTask(text)}></TextInput>
        <TouchableOpacity onPress={() => { handleAddTask() }}>
          <View style={styles.addWrapper}>
            <Text style={styles.addText}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
    alignContent: 'center',
    alignItems: 'center'
    // marginTop: Constants.statusBarHeight,
  },
  taskWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,

  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  items: {
    marginTop: 30,
  },
  writeTaskWrapper: {
    position: 'absolute',
    bottom: 60,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  input: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: '#FFF',
    borderRadius: 60,
    borderColor: '#c0c0c0',
    borderWidth: 1,
    width: '70%',
  },
  addWrapper: {
    width: 60,
    height: 60,
    backgroundColor: '#FFF',
    borderRadius: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#c0c0c0',
    borderWidth: 1,
  },
  addText: {
    fontSize: 24,
    color: '#c0c0c0'
  }
});
